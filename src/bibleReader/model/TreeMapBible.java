package bibleReader.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author ? (provided the implementation)
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String						version;
	private TreeMap<Reference, Verse>	theVerses;
	private String title;
	private Set<Reference> refs;
	
	
	// Or replace the above with:
	// private TreeMap<Reference, Verse> theVerses;
	// Add more fields as necessary.

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses) {
		// TODO Implement me: Stage 11
		theVerses = new TreeMap<Reference, Verse>();
		for (Verse verse: verses) {
		theVerses.put(verse.getReference(), verse);
		}
		version = verses.getVersion();
		title = verses.getDescription();
		refs = theVerses.keySet();
	}

	@Override
	public int getNumberOfVerses() {
		// TODO Implement me: Stage 11
		return theVerses.size();
	}

	@Override
	public VerseList getAllVerses() {
		// TODO Implement me: Stage 11
		return new VerseList(version, title, theVerses.values());
	}

	@Override
	public String getVersion() {
		// TODO Implement me: Stage 11
		return version;
	}

	@Override
	public String getTitle() {
		// TODO Implement me: Stage 11
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		// TODO Implement me: Stage 11
		if (ref.getBookOfBible() != null) {
			VerseList list = new VerseList(version, title, theVerses.values());
			return list.indexOfVerseWithReference(ref) != -1;
		}
		else {
		return false;
		}
	}

	@Override
	public String getVerseText(Reference r) {
		// TODO Implement me: Stage 11
		if (theVerses.containsKey(r)) {
		return theVerses.get(r).getText();
		}
		else {
			return null;
		}
	}

	@Override
	public Verse getVerse(Reference r) {
		// TODO Implement me: Stage 11
		return theVerses.get(r);
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		// TODO Implement me: Stage 11
		Reference r = new Reference(book, chapter, verse);
		return getVerse(r);
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	//
	// For Stage 11 the first two methods below will be implemented as specified in the comments.
	// Do not over think these methods. All three should be pretty straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they work better.
	// At that stage you will create another class to facilitate searching and use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getVersesContaining(String phrase) {
		ArrayList<Verse> list = new ArrayList<Verse>();
		if (phrase.equals("")) {
			return new VerseList(version, phrase, list);
		}
		String search = phrase.trim().toLowerCase();
		for (Reference ref : refs) {
			if (theVerses.get(ref).getText().trim().toLowerCase().contains(search)) {
				list.add(theVerses.get(ref));
			}
		}
		return new VerseList(version, phrase, list);
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> ref = new ArrayList<Reference>();
		if (phrase.equals("")) {
			return ref;
		}
		String search = phrase.trim().toLowerCase();
		for (Reference reference : refs) {
			if (theVerses.get(reference).getText().trim().toLowerCase().contains(search)) {
				ref.add(reference);
			}
		}
		return ref;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		// TODO Implement me: Stage 11
		ArrayList<Verse> list = new ArrayList<Verse>();		
		for (Reference ref: references) {
			if (ref.getBookOfBible() != null) {
				list.add(getVerse(ref));
			}
			else {
				list.add(null);
			}
		}
		return new VerseList(version, "Arbitrary list of Verses", list);
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 11.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 11
		int verseCount = 0;
		if (isValid(new Reference(book, chapter, 1))) {
			for (Reference ref: refs) {
				BookOfBible refBook = ref.getBookOfBible();
				int refChapter = ref.getChapter();
				if (refBook.equals(book) && refChapter == chapter && ref.getVerse() > verseCount) {
					verseCount = ref.getVerse();
				} 
				else if (verseCount > 0 && (!refBook.equals(book) || refChapter != chapter)) {
					return verseCount;
				}
			}
			return verseCount;
		}				
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 11
		int chapterCount = 0;
		if (isValid(new Reference(book, 1, 1))) {
			for (Reference ref : refs) {
				BookOfBible refBook = ref.getBookOfBible();
				if (refBook.equals(book) && ref.getChapter() > chapterCount) {
					chapterCount = ref.getChapter();
				}
				else if (chapterCount > 0 && !refBook.equals(book)) {
					return chapterCount;
				}
			}
			return chapterCount;
		}			
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 11
		ArrayList<Reference> list = new ArrayList<Reference>();
		if (isValid(firstVerse) && isValid(lastVerse) && firstVerse.compareTo(lastVerse) <=0) {
			for (Reference ref : refs) {
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <= 0) {
					list.add(ref);
				}
			}
		}
		return list;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 11
		ArrayList<Reference> list = new ArrayList<Reference>();
		if (isValid(firstVerse) && isValid(lastVerse)) {
			for (Reference ref : refs) {
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) < 0) {
					list.add(ref);
				}
			}
		}
		return list;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		// TODO Implement me: Stage 11
		if (book != null) {
			return getReferencesExclusive(new Reference(book, 1, 1), new Reference(BookOfBible.nextBook(book), 1, 1));
		}
		else {
			return new ArrayList<Reference>();
		}
	}		

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 11
		return getReferencesExclusive(new Reference(book, chapter, 1), new Reference(book, chapter + 1, 1));
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 11
		return getReferencesExclusive(new Reference(book, chapter1, 1), new Reference(book, chapter2 + 1, 1));
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 11
		return getReferencesInclusive(new Reference(book, chapter, verse1), new Reference(book, chapter, verse2));
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 11
		return getReferencesInclusive(new Reference(book, chapter1, verse1), new Reference(book, chapter2, verse2));
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		// TODO Implement me: Stage 11
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);
		Reference trueLastVerse = new Reference(lastVerse.getBookOfBible(), lastVerse.getChapter(), lastVerse.getVerse() + 1);
		// Make sure the references are in the correct order. If not, return an empty list.
		if (firstVerse.compareTo(trueLastVerse) > 0) {
			return someVerses;
		}
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, trueLastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// Implementation of this method provided by Chuck Cusack.
		// This is provided so you have an example to help you get started
		// with the other methods.

		// We will store the resulting verses here. We copy the version from
		// this Bible and set the description to be the passage that was searched for.
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);

		// Make sure the references are in the correct order. If not, return an empty list.
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			someVerses.add(element.getValue());
		}
		return someVerses;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		// TODO Implement me: Stage 11
		ArrayList<Verse> list = new ArrayList<Verse>();
		for (Reference ref : refs) {
			if (ref.getBookOfBible().equals(book)) {
				list.add(theVerses.get(ref));
			}
		}
		return new VerseList(version, "<i>" + book + "</i>", list);
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 11
		ArrayList<Verse> list = new ArrayList<Verse>();
		for (Reference ref : refs) {
			if (ref.getBookOfBible().equals(book) && ref.getChapter() == chapter) {
				list.add(theVerses.get(ref));
			}
		}
		return new VerseList(version, "" + book + chapter, list);
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 11
		ArrayList<Verse> list = new ArrayList<Verse>();
		Reference firstVerse = new Reference(book, chapter1, 1);
		Reference lastVerse = new Reference(book, chapter2 + 1, 1);
		if (isValid(firstVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Reference ref : refs) {
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) < 0) {
					list.add(theVerses.get(ref));
				}
			}
		}
		return new VerseList(version, "" + book + chapter1 + "-" + chapter2, list);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 11
		ArrayList<Verse> list = new ArrayList<Verse>();
		Reference firstVerse = new Reference(book, chapter, verse1);
		Reference lastVerse = new Reference(book, chapter, verse2);
		if (isValid(firstVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Reference ref : refs) {
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <= 0) {
					list.add(theVerses.get(ref));
				}
			}
		}
		return new VerseList(version, "" + book + chapter + ":" + verse1 + "-" + verse2, list);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 11
		ArrayList<Verse> list = new ArrayList<Verse>();
		Reference firstVerse = new Reference(book, chapter1, verse1);
		Reference lastVerse = new Reference(book, chapter2, verse2);
		if (isValid(firstVerse) && firstVerse.compareTo(lastVerse) <= 0) {
			for (Reference ref : refs) {
				if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <= 0) {
					list.add(theVerses.get(ref));
				}
			}
		}
		return new VerseList(version, "" + book + chapter1 + ":" + verse1 + "-" + chapter2 + ":" + verse2, list);
	}

}
