package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for searching for verses based on words or
 * references.
 * 
 * @author cusack
 * @author Daniel Keith
 * @author Evan Mulshine
 * @author Dre Solorzano
 */
public class BibleReaderModel implements MultiBibleModel {

	private ArrayList<Bible> bibles;
	
	// Patterns for parsing passage text.
	public static final String number = "\\s*(\\d+)\\s*";
	public static Pattern bookPattern = Pattern.compile("\\s*((?:1|2|3|I|II|III)\\s*\\w+|(?:\\s*[a-zA-Z]+)+)\\s*(.*)");
	public static Pattern cPattern = Pattern.compile(number);
	public static Pattern cvcvPattern = Pattern.compile(number + ":" + number + "-" + number + ":" + number);
	public static Pattern ccPattern = Pattern.compile(number + "-" + number);
	public static Pattern ccvPattern = Pattern.compile(number + "-" + number + ":" + number);
	public static Pattern cvvPattern = Pattern.compile(number + ":" + number + "-" + number);
	public static Pattern cvPattern = Pattern.compile(number + ":" + number);

	/**
	 * Default constructor.
	 */
	public BibleReaderModel() {
		bibles = new ArrayList<Bible>();
	}

	@Override
	public String[] getVersions() {
		String[] versions = new String[bibles.size()];
		int index = 0;
		for (Bible bible : bibles) {
			versions[index] = bible.getVersion();
			index++;
		}
		Arrays.sort(versions);
		return versions;
	}

	@Override
	public int getNumberOfVersions() {
		return bibles.size();
	}

	@Override
	public void addBible(Bible bible) {
		bibles.add(bible);
	}

	@Override
	public Bible getBible(String version) {
		for (Bible bible : bibles) {
			if (bible.getVersion().toLowerCase().equals(version.toLowerCase())) {
				return bible;
			}
		}
		return null;

	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String words) {
		TreeSet<Reference> refs = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			refs.addAll(bible.getReferencesContaining(words));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public VerseList getVerses(String version, ArrayList<Reference> references) {
		return getBible(version).getVerses(references);
	}
	//---------------------------------------------------------------------

	@Override
	public String getText(String version, Reference reference) {
		for (Bible bible : bibles) {
			if(bible.getVersion().equals(version)) {
				String text = bible.getVerseText(reference);
				if (text != null) {
					return text;
				}
			}
		}
		return "";
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		String theRest = null;
		String book = null;
		BookOfBible bibleBook = null;
		int chapter1, chapter2, verse1, verse2;

		// First, split the input into the book and the rest, if possible.
		Matcher m = bookPattern.matcher(reference);

		// Now see if it matches.
		if (m.matches()) {
			// It matches.  Good.
			book = m.group(1);
			bibleBook = BookOfBible.getBookOfBible(book);
			theRest = m.group(2);
			// Now we need to parse theRest to see what format it is.
			if (theRest.length() == 0) {
				// It looks like they want a whole book.
				return getBookReferences(bibleBook);
			} else if ((m = cvcvPattern.matcher(theRest)).matches()) {
				// They want something of the form book chapter1:verse1-chapter2:verse2
				chapter1 = Integer.parseInt(m.group(1));
				verse1 = Integer.parseInt(m.group(2));
				chapter2 = Integer.parseInt(m.group(3));
				verse2 = Integer.parseInt(m.group(4));
				return getPassageReferences(bibleBook, chapter1, verse1, chapter2, verse2);
			} else if ((m = ccPattern.matcher(theRest)).matches()) {
				// They want something of the form book chapter1-chapter2
				chapter1 = Integer.parseInt(m.group(1));
				chapter2 = Integer.parseInt(m.group(2));
				return getChapterReferences(bibleBook, chapter1, chapter2);
			} else if ((m = cPattern.matcher(theRest)).matches()) {
				// They want something of the form book chapter1
				chapter1 = Integer.parseInt(m.group(1));
				return getChapterReferences(bibleBook, chapter1);
			} else if ((m = ccvPattern.matcher(theRest)).matches()) {
				// They want something of the form book chapter1-chapter2:verse2
				chapter1 = Integer.parseInt(m.group(1));
				chapter2 = Integer.parseInt(m.group(2));
				verse2 = Integer.parseInt(m.group(3));
				return getPassageReferences(bibleBook, chapter1, 1, chapter2, verse2);
			} else if ((m = cvvPattern.matcher(theRest)).matches()) {
				// They want something of the form book chapter1:verse1-verse2
				chapter1 = Integer.parseInt(m.group(1));
				verse1 = Integer.parseInt(m.group(2));
				verse2 = Integer.parseInt(m.group(3));
				return getPassageReferences(bibleBook, chapter1, verse1, verse2);
			} else if ((m = cvPattern.matcher(theRest)).matches()) {
				// They want something of the form book chapter1:verse1
				chapter1 = Integer.parseInt(m.group(1));
				verse1 = Integer.parseInt(m.group(2));
				return getVerseReferences(bibleBook, chapter1, verse1);
			}
		}
		// No matches found, so return an empty list
		return new ArrayList<Reference>();
	}

	@Override
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		Reference ref = new Reference(book, chapter, verse);
		for (Bible bible : bibles) {
			if (bible.isValid(ref)) {
				refs.add(ref);
				return refs;
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		TreeSet<Reference> refs = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			refs.addAll(bible.getReferencesInclusive(startVerse, endVerse));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		TreeSet<Reference> refs = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			refs.addAll(bible.getReferencesForBook(book));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		TreeSet<Reference> refs = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			refs.addAll(bible.getReferencesForChapter(book, chapter));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		TreeSet<Reference> refs = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			refs.addAll(bible.getReferencesForChapters(book, chapter1, chapter2));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		TreeSet<Reference> refs = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			refs.addAll(bible.getReferencesForPassage(book, chapter, verse1, verse2));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		TreeSet<Reference> refs = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			refs.addAll(bible.getReferencesForPassage(book, chapter1, verse1, chapter2, verse2));
		}
		return new ArrayList<Reference>(refs);
	}
	
	//------------------------------------------------------------------
	// These are the better searching methods.
	// 
	@Override
	public ArrayList<Reference> getReferencesContainingWord(String word) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		// TODO Implement me: Stage 12
		return null;
	}
}
