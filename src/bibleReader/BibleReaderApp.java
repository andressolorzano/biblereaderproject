package bibleReader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 * @author Daniel Keith
 * @author Evan Mulshine
 * @author Dre Solorzano
 */
public class BibleReaderApp extends JFrame {
	public static final int width = 800;
	public static final int height = 600;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	// Fields
	private BibleReaderModel model;
	private ResultView resultView;
	public JTextField inputField;
	public JButton phraseButton;
	public JButton passageButton;
	private JPanel searchPanel;
	private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu helpMenu;
    private JMenuItem exitItem;
    private JMenuItem openItem;
    private JMenuItem aboutItem;
    private JFileChooser fc;


	/**
	 * Create a BibleReaderApp.
	 */
	public BibleReaderApp() {
		model = new BibleReaderModel();
		
		File kjvFile = new File("kjv.atv");
		VerseList versesKJV = BibleIO.readBible(kjvFile);
		Bible kjv = new ArrayListBible(versesKJV);
		model.addBible(kjv);
		
		File asvFile = new File("asv.xmv");
		VerseList versesASV = BibleIO.readBible(asvFile);
		Bible asv = new ArrayListBible(versesASV);
		model.addBible(asv);
		
		File esvFile = new File("esv.atv");
		VerseList versesESV = BibleIO.readBible(esvFile);
		Bible esv = new ArrayListBible(versesESV);
		model.addBible(esv);
		
		resultView = new ResultView(model);

		setupLookAndFeel();
		setupGUI();
		pack();
		setSize(width, height);

		// So the application exits when you click the "x".
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/** 
	 * Enhances the look of the application.
	 */
	private void setupLookAndFeel() {
		UIManager.put("control", new Color(200,200,200));
		UIManager.put("nimbusLightBackground", new Color(220,220,220));
		UIManager.put("nimbusFocus", new Color(150,150,150));
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// It will use the default look and feel.
		}
	}
	/**
	 * Set up the main GUI.
	 */
	private void setupGUI() {
		setTitle("Bible Reader");
		
		// Initialize input field and button.
		inputField = new JTextField(30);
		phraseButton = new JButton("Search for phrase");
		passageButton = new JButton("Search for passage");
		
		// Set up ActionListeners.
		inputField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultView.search(inputField.getText());
			}
		});
		phraseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultView.search(inputField.getText());
			}
		});
		passageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultView.passage(inputField.getText());
			}
		});
		// Put input field and button into one panel.
		searchPanel = new JPanel();
		searchPanel.add(inputField);
		searchPanel.add(phraseButton);
		searchPanel.add(passageButton);
		
		// Set up the menu.
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
        
        helpMenu = new JMenu("Help");
        menuBar.add(helpMenu);
        
        // Add an exit button to the File menu.
        exitItem = new JMenuItem("Exit");
        fileMenu.add(exitItem);
        exitItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		System.exit(0);
        	}
        });
        
        // Add an open button to the file menu.
        openItem = new JMenuItem("Open");
        fileMenu.add(openItem);
        openItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		openFile();
        		resultView.refresh();
        	}
        });
        
        // Add an about button to the Help menu.
        aboutItem = new JMenuItem("About");
        helpMenu.add(aboutItem);
        aboutItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		JOptionPane.showMessageDialog(BibleReaderApp.this, "A simple Bible reader application.  Interfaces and some implementation"
        				+ " provided by Chuck Cusack.  Implementation provided by Evan Mulshine, Dre Solorzano, and Daniel Keith.");
        	}
        });
		// Add everything to the main window.
		Container contents = getContentPane();
		contents.setLayout(new BorderLayout());
		contents.add(searchPanel, BorderLayout.NORTH);
		contents.add(resultView, BorderLayout.CENTER);
		
		// Set element names for the test.
		inputField.setName("InputTextField");
		phraseButton.setName("SearchButton");
		passageButton.setName("PassageButton");

		

		// The stage numbers below may change, so make sure to pay attention to
		// what the assignment says.
		// TODO Add passage lookup: Stage ?
		// TODO Add 2nd version on display: Stage ?
		// TODO Limit the displayed search results to 20 at a time: Stage ?
		// TODO Add 3rd versions on display: Stage ?
		// TODO Format results better: Stage ?
		// TODO Display cross references for third version: Stage ?
		// TODO Save/load search results: Stage ?
	}
	
	/**
	 * Open and read a bible file
	 */
	private void openFile() {
		if (fc == null) {
			fc = new JFileChooser();
		}
		int returnVal = fc.showDialog(this, "Open");
		// We check whether or not they clicked the "Open" button
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			// We get a reference to the file that the user selected.
			File file = fc.getSelectedFile();
			// Make sure it actually exists.
			if (!file.exists()) {
				JOptionPane.showMessageDialog(this, "That file does not exist!",
						"File not found", JOptionPane.ERROR_MESSAGE);
			// Read the file
			} else {
				VerseList verses = BibleIO.readBible(file);
				Bible bible = new ArrayListBible(verses);
				model.addBible(bible);
			}
		}
	}

}
